<?
include("include/misc.php");
include("include/connect.php"); 
session_start();

$ip =  getenv("REMOTE_ADDR");
$session_id = session_id();

//$QRId = GetRequest("QRId");
$QRId = "TMB_TLS_TESTING";

$inq_url = "https://sit.datagateway.tmbbizdirect.com:9643/VerifySlip/dgw/VerifyV2/QRPayment/SCI";
//$inq_url = "https://datagateway.tmbbank.com:9643/VerifySlip/dgw/VerifyV2/QRPayment/SCI";
$ip =  getenv("REMOTE_ADDR");

if ($QRId != "") {
	$BillerNo = "010755500059702";
	$data_json = '{"BillerNo":"' . $BillerNo. '","QRId":"' . $QRId . '"}';
	writeLog($ip . "\tVRF\t" . $inq_url . "\r\n" . $data_json . "\r\n");

	$sql = "INSERT INTO TXN_LOG_TMBQR (LOG_DATE, LOG_TYPE, REQUEST, RESPONSE, URL, IP_ADDR, SESSION_ID";
	$sql .= ", BankRef, BillerNo, Ref1, Ref2, QRId, PayerName, PayerBank, Filler, Amount, Fee, ResultCode, ResultDesc, TransDate) VALUES (";
	$sql .= "NOW(), 'VRF', '" . $data_json . "', '', '". $inq_url . "', '". $ip . "', '" . $session_id . "'";
	$sql .= ", '" . $BankRef . "', '". $BillerNo . "', '". $Ref1 ."', '".$Ref2."', '".$QRId."'";
	$sql .= ", '".$PayerName."', '".$PayerBank."', '".$Filler."', '".$Amount."', '".$Fee."', '".$ResultCode."', '".$ResultDesc."','".$TransDate."')";
	mysql_unbuffered_query($sql);
	writeLog($ip . "\tINS_LOG\t" . $sql ."\r\n");

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $inq_url);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_json))
	);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 300);
	curl_setopt($ch, CURLOPT_POST, true);

	$res = curl_exec($ch);
	if ($res != "") {
		$data_res = json_decode($res);
		$BankRef = $data_res->bankRef;
		$BillerNo = $data_res->billerNo;
		$Ref1 = $data_res->ref1;
		$Ref2 = $data_res->ref2;
		$QRId = $data_res->qRId;
		$PayerName = $data_res->payerName;
		$PayerBank = $data_res->payerBank;
		$Amount = $data_res->amount;
		$Fee = $data_res->fee;
		$ResultCode = $data_res->resultCode;
		$ResultDesc = $data_res->resultDesc;
		$TransDate = $data_res->transDate;

		writeLog($ip . "\tRVF\t" . $inq_url . "\r\n" . $res . "\r\n");
		
		$sql = "INSERT INTO TXN_LOG_TMBQR (LOG_DATE, LOG_TYPE, REQUEST, RESPONSE, URL, IP_ADDR, SESSION_ID";
		$sql .= ", BankRef, BillerNo, Ref1, Ref2, QRId, PayerName, PayerBank, Filler, Amount, Fee, ResCode, ResDesc, TransDate) VALUES (";
		$sql .= "NOW(), 'RVF', '', '" . $res . "', '". $inq_url . "', '". $ip . "', '" . $session_id . "'";
		$sql .= ", '" . $BankRef . "', '". $BillerNo . "', '". $Ref1 ."', '".$Ref2."', '".$QRId."'";
		$sql .= ", '".$PayerName."', '".$PayerBank."', '', '".$Amount."', '".$Fee."', '".$ResultCode."', '" . $ResultDesc . "','".$TransDate."')";
		mysql_unbuffered_query($sql);

		writeLog($ip . "\tINS_LOG\t" . $sql ."\r\n");

	}

}
if (isJSON($res)) {
	header("Content-Type: application/json; charset=utf-8");
	echo $res;
} else {
	echo $res;
}
?>