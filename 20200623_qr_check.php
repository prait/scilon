<?
include("include/misc.php");
include("include/connect.php"); 
header("Content-Type: application/json; charset=utf-8");
session_start();

$url = "https://www.siamcityinsurance.com:9950/qr_check.php";
$ip =  getenv("REMOTE_ADDR");
$session_id = session_id();
$json = file_get_contents('php://input');
$data_json = "{}";

if ($json != "") {
	writeLogFile("tmbqr", $ip . "\tREQ\r\n" . $json . "\r\n");

	$data = json_decode($json);

	$BankRef = $data->BankRef;
	$BillerNo = $data->BillerNo;
	$Ref1 = $data->Ref1;
	$Ref2 = $data->Ref2;
	$QRId = $data->QRId;
	$PayerName = $data->PayerName;
	$PayerBank = $data->PayerBank;
	$Filler = $data->Filler;
	$Amount = $data->Amount;
	$Fee = $data->Fee;
	$ResultCode = $data->ResultCode;
	$ResultDesc = $data->ResultDesc;
	$TransDate = $data->TransDate;

	$sql = "INSERT INTO TXN_LOG_TMBQR (LOG_DATE, LOG_TYPE, REQUEST, RESPONSE, URL, IP_ADDR, SESSION_ID";
	$sql .= ", BankRef, BillerNo, Ref1, Ref2, QRId, PayerName, PayerBank, Filler, Amount, Fee, ResultCode, ResultDesc, TransDate) VALUES (";
	$sql .= "NOW(), 'REQ', '" . $json . "', '', '". $url . "', '". $ip . "', '" . $session_id . "'";
	$sql .= ", '" . $BankRef . "', '". $BillerNo . "', '". $Ref1 ."', '".$Ref2."', '".$QRId."'";
	$sql .= ", '".$PayerName."', '".$PayerBank."', '".$Filler."', '".$Amount."', '".$Fee."', '".$ResultCode."', '".$ResultDesc."','".$TransDate."')";
	mysql_unbuffered_query($sql);

	if (($BankRef != "") && ($TransDate != "")) {
		$data_res->BankRef = $BankRef;
		$data_res->ResCode = "000";
		$data_res->ResDesc = "Success";
		$data_res->TransDate = $TransDate;

		$data_json = json_encode($data_res);
		writeLogFile("tmpqr", $ip . "\tRES\r\n" . $data_json . "\r\n");

		$sql = "INSERT INTO TXN_LOG_TMBQR (LOG_DATE, LOG_TYPE, REQUEST, RESPONSE, URL, IP_ADDR, SESSION_ID";
		$sql .= ", BankRef, BillerNo, Ref1, Ref2, QRId, PayerName, PayerBank, Filler, Amount, Fee, ResCode, ResDesc, TransDate) VALUES (";
		$sql .= "NOW(), 'RES', '', '" . $data_json . "', '". $url . "', '". $ip . "', '" . $session_id . "'";
		$sql .= ", '" . $data_res->BankRef . "', '". $BillerNo . "', '". $Ref1 ."', '".$Ref2."', '".$QRId."'";
		$sql .= ", '".$PayerName."', '".$PayerBank."', '".$Filler."', '".$Amount."', '".$Fee."', '".$data_res->ResCode."', '".$data_res->ResDesc."','".$data_res->TransDate."')";
		mysql_unbuffered_query($sql);
	}

	if ($ResultCode = "000") {		// QR Payment Success
		$appId =	"SCI-EC" . substr($Ref1, 5, 2) ."-" . substr($Ref1, -6);			//SCIEC20000243 => SCI-EC20-000243

		//-------- RUNNING POLICY ------------//
		$sql_order		= "select * from scil_website.mas_policy_order_temp where appId ='" . $appId . "'";
		$query_order	= mysql_query($sql_order) or die (mysql_error());
		$row_order = mysql_fetch_array($query_order);
		
		$sql_soap = "select * from scil_website.mas_policy_soap_temp where appId='" .$appId . "' ";
		$query_soap = mysql_query($sql_soap) or die (mysql_error());
		$row_soap = mysql_fetch_array($query_soap);

		if (($row_soap) && ($row_order)) {
			$policy_type	=	$row_soap["subClass"];
			if($row_soap["minorSrc"]=='SC') {
				$branch_id = "SC1" ;
			} else {
				$branch_id = "HO1" ;
			}

			$sql = "SELECT * FROM scil.MAS_RUNNING";
			$sql .= " WHERE POLICY_GROUP = '".$policy_type."' ";
			$sql .= " AND BRANCH_ID = '".$branch_id."' ";
			$sql .= " AND YEARS = '".date("Y")."'";
			$result = mysql_query($sql);
			if ($rs = mysql_fetch_array($result)) {
				$running = $rs["RUNNING_NUMBER"] + 1;
				$policy_id = $branch_id . "-" . date("y")."-M".$policy_type."-". substr("000000".strval($running), -6);
		
				$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
				$sql .= " WHERE POLICY_GROUP = '".$policy_type."'";
				$sql .= " AND BRANCH_ID = '".$branch_id."' ";
				$sql .= " AND YEARS = '".date("Y")."'";
				mysql_unbuffered_query($sql);
			} else {
				$policy_id = $branch_id . "-" .date("y")."-M".$policy_type."-000001";
		
				$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','HO1','".$policy_type."','1')";
				mysql_unbuffered_query($sql);
			}

			//----------- RECEIPT POLICY -------------//
			if(($policy_type=="MA1") || ($policy_type=="MA2")) {
				$sql = "SELECT * FROM scil.MAS_RUNNING";
				$sql .= " WHERE POLICY_GROUP = 'BARCODE'";
				$sql .= " AND YEARS = '".date("Y")."'";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$running = $rs["RUNNING_NUMBER"] + 1;
		
					$sticker_id = "06". date("y")."9".substr("0000000".strval($running), -7) . substr(strval($running), -1);
		
					$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
					$sql .= " WHERE POLICY_GROUP = 'BARCODE'";
					$sql .= " AND YEARS = '".date("Y")."'";
					mysql_query($sql);
				} else {
					$sticker_id = "06". date("y")."900000011";
		
					$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','','BARCODE','1')";
					mysql_unbuffered_query($sql);
				}
				
				$sql = "SELECT * FROM scil.MAS_RUNNING";
				$sql .= " WHERE POLICY_GROUP = 'TAXVOICE'";
				$sql .= " AND YEARS = '".date("Y")."'";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$running = $rs["RUNNING_NUMBER"] + 1;

					$receipt_id = "MMA". date("y").substr("000000".strval($running), -6);

					$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
					$sql .= " WHERE POLICY_GROUP = 'TAXVOICE'";
					$sql .= " AND YEARS = '".date("Y")."'";
					mysql_unbuffered_query($sql);
				} else {
					$receipt_id = "MMA". date("y")."000001";

					$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','','TAXVOICE','1')";
					mysql_unbuffered_query($sql);
				}

				$sql = "SELECT * FROM scil.MAS_RUNNING";
				$sql .= " WHERE POLICY_GROUP = 'INVOICE'";
				$sql .= " AND YEARS = '".date("Y")."'";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$running = $rs["RUNNING_NUMBER"] + 1;

					$invoice_id = "1MA". date("y").substr("000000".strval($running), -6);

					$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
					$sql .= " WHERE POLICY_GROUP = 'INVOICE'";
					$sql .= " AND YEARS = '".date("Y")."'";
					mysql_unbuffered_query($sql);
				} else {
					$invoice_id = "1MA". date("y")."000001";

					$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','','INVOICE','1')";
					mysql_unbuffered_query($sql);
				}
			} else {											// Voluntary
				$sql = "SELECT * FROM scil.MAS_RUNNING";
				$sql .= " WHERE POLICY_GROUP = 'TAX_INVOICE_VOL'";
				$sql .= " AND YEARS = '".date("Y")."'";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$running = $rs["RUNNING_NUMBER"] + 1;
			
					$receipt_id = "MMV". date("y").substr("000000".strval($running), -6);
			
					$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
					$sql .= " WHERE POLICY_GROUP = 'TAX_INVOICE_VOL'";
					$sql .= " AND YEARS = '".date("Y")."'";
					mysql_unbuffered_query($sql);
				} else {
					$receipt_id = "MMV". date("y")."000001";
			
					$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','','TAX_INVOICE_VOL','1')";
					mysql_unbuffered_query($sql);
				}
			
				$sql = "SELECT * FROM scil.MAS_RUNNING";
				$sql .= " WHERE POLICY_GROUP = 'INVOICE_VOL'";
				$sql .= " AND YEARS = '".date("Y")."'";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$running = $rs["RUNNING_NUMBER"] + 1;
			
					$invoice_id = "1MV". date("y").substr("000000".strval($running), -6);
			
					$sql = "UPDATE scil.MAS_RUNNING SET RUNNING_NUMBER = '". strval($running)."'";
					$sql .= " WHERE POLICY_GROUP = 'INVOICE_VOL'";
					$sql .= " AND YEARS = '".date("Y")."'";
					mysql_unbuffered_query($sql);
				} else {
					$invoice_id = "1MV". date("y")."000001";
			
					$sql = "INSERT INTO scil.MAS_RUNNING (YEARS, BRANCH_ID, POLICY_GROUP, RUNNING_NUMBER) VALUES ('".date("Y")."','','INVOICE_VOL','1')";
					mysql_unbuffered_query($sql);
				}
			}

			// ------- TEMPORARY RECEIPT --------//            
			$sql		= "select * from scil_temporary_receipt.mas_running";
			$sql		.= " where bill_code= 'TRD'";
			$sql		.= " and  payment_group = 'PAYMENT_TRD'";
			$sql		.= " and years  = '".date("Y")."'";
			$result	= mysql_query($sql) or die (mysql_error());
			if ($rs = mysql_fetch_array($result)) {
				$running = $rs["running_number"] + 1;

				$refer_no = "TRD-". date("y-").substr("000000".strval($running), -6) . substr(strval($running), -1).'-01';

				$sql = "UPDATE scil_temporary_receipt.mas_running set running_number = '". strval($running)."'";
				$sql	.= " where bill_code= 'TRD'";
				$sql	.= " and  payment_group = 'PAYMENT_TRD'";
				$sql	.= " and years  = '".date("Y")."'";
				mysql_unbuffered_query($sql);
			} else {
				$refer_no = "TRD-". date("y")."-0000011-01";
				
				$year = date("Y");

				$sql = "insert into scil_temporary_receipt.mas_running set
							years					=	'$year',	
							bill_code			=	'TRD',	
							payment_group	=	'PAYMENT_TRD',	
							running_number	= 	'1'";																															
				mysql_query($sql) or die (mysql_error());	
			}

			//-------- INSERT MAS POLICY ORDER ------------//
			$date_order	= 	date('Y-m-d H:i:s');
			$ip_order		=	$_SERVER['REMOTE_ADDR'];	
			$sql_order = "insert into scil_website.mas_policy_order set 
								appId				= '$row_order[appId]',	
								title_order			= '$row_order[title_order]',	
								fname_order		= '$row_order[fname_order]',	
								lname_order		= '$row_order[lname_order]',	
								address_order	= '$row_order[address_order]',
								district_code		= '$row_order[district_code]',	
								district_order		= '$row_order[district_order]',	
								amphur_code		= '$row_order[amphur_code]',	
								amphur_order	= '$row_order[amphur_order]',	
								province_code	= '$row_order[province_code]',	
								province_order	= '$row_order[province_order]',	
								postcode_order	= '$row_order[postcode_order]',	
								phone_order		= '$row_order[phone_order]',	
								email_order		= '$row_order[email_order]',
								send_policy		=	'$row_order[send_policy]',
								add_policy			=	'$row_order[add_policy]',
								addno_policy		=	'$row_order[addno_policy]',
								district_policy		=	'$row_order[district_policy]',
								amphur_policy	=	'$row_order[amphur_policy]',
								province_policy	=	'$row_order[province_policy]',
								postcode_policy	=	'$row_order[postcode_policy]',
								approve_order	=	'$row_order[approve_order]',
								invoice_pgw		=	'$row_order[invoice_pgw]',
								date_order			= '$date_order',	
								promotion			= '$row_order[promotion]',	
								promotion_code	= '$row_order[promotion_code]',	
								security_code		= '$row_order[security_code]',	
								referralcode		=	'$row_order[referralcode]',
								uuid					=	'$row_order[uuid]',
								ip_order			= '$ip_order'";		
			mysql_query($sql_order) or die (mysql_error());	
			//-------- / INSERT MAS POLICY ORDER ------------//
		
			//-------- INSERT MAS POLICY SOAP ------------//
			$uwYear = date('Y');
			$tmp01 = date('d/m/Y');
			$sql_soap	 = "insert into scil_website.mas_policy_soap set 
								 addrAmpCd		=	'$row_soap[addrAmpCd]',
								 addrAmpNm		=	'$row_soap[addrAmpNm]',
								 addrBld				=	'$row_soap[addrBld]',
								 addrChwCd		=	'$row_soap[addrChwCd]',
								 addrChwNm		=	'$row_soap[addrChwNm]',	
								 addrMoo			=	'$row_soap[addrMoo]',
								 addrNo				=	'$row_soap[addrNo]',
								 addrPostcode	=	'$row_soap[addrPostcode]',
								 addrRoad			=	'$row_soap[addrRoad]',
								 addrSoi			=	'$row_soap[addrSoi]',
								 addrThmCd		=	'$row_soap[addrThmCd]',
								 addrThmNm		=	'$row_soap[addrThmNm]',
								 appId				=	'$row_soap[appId]',
								 appType			=	'$row_soap[appType]',
								 benefitNm			=	'$row_soap[benefitNm]',
								 bodyDesc			=	'$row_soap[bodyDesc]',
								 cardId				=	'$row_soap[cardId]',
								 carGroup			=	'$row_soap[carGroup]',
								 carModify			=	'$row_soap[carModify]',
								 cbarcodeRef		=	'$row_soap[cbarcodeRef]',
								 cc					=	'$row_soap[cc]',
								 ccovBody			=	'$row_soap[ccovBody]',
								 ccovLife			=	'$row_soap[ccovLife]',
								 ccovPerson		=	'$row_soap[ccovPerson]',
								 ccovTimes		=	'$row_soap[ccovTimes]',
								 ceffectDt			=	'$row_soap[ceffectDt]',
								 cexpiryDt			=	'$row_soap[cexpiryDt]',
								 chasNo				=	'$row_soap[chasNo]',
								 cnetPrmm			=	'$row_soap[cnetPrmm]',
								 coldPolicy			=	'$row_soap[coldPolicy]',
								 colorDesc			=	'$row_soap[colorDesc]',
								 comGrp			=	'$row_soap[comGrp]',
								 coverCd			=	'$row_soap[coverCd]',
								 cpolicyNo			=	'$row_soap[cpolicyNo]',
								 creference		=	'$row_soap[creference]',
								 cstampDuty		=	'$row_soap[cstampDuty]',
								 csvctaxAmt		=	'$row_soap[csvctaxAmt]',
								 ctotalPrmm		=	'$row_soap[ctotalPrmm]',
								 ctype				=	'$row_soap[ctype]',
								 drvBirthDt1		=	'$row_soap[drvBirthDt1]',
								 drvBirthDt2		=	'$row_soap[drvBirthDt2]',
								 drvCardId1		=	'$row_soap[drvCardId1]',
								 drvCardId2		=	'$row_soap[drvCardId2]',
								 drvDriveId1		=	'$row_soap[drvDriveId1]',
								 drvDriveId2		=	'$row_soap[drvDriveId2]',
								 drvNm1			=	'$row_soap[drvNm1]',
								 drvNm2			=	'$row_soap[drvNm2]',
								 drvOccDesc1		=	'$row_soap[drvOccDesc1]',
								 drvOccDesc2		=	'$row_soap[drvOccDesc2]',
								 drvSex1			=	'$row_soap[drvSex1]',
								 drvSex2			=	'$row_soap[drvSex2]',
								 emailAddr			=	'$row_soap[emailAddr]',
								 engineNo			=	'$row_soap[engineNo]',
								 faxNo				=	'$row_soap[faxNo]',
								 firstNm				=	'$row_soap[firstNm]',
								 intmCard			=	'$row_soap[intmCard]',
								 intmId				=	'$row_soap[intmId]',
								 lastNm				=	'$row_soap[lastNm]',
								 mainClass			=	'$row_soap[mainClass]',
								 makeDesc			=	'$row_soap[makeDesc]',
								 minorSrc			=	'$row_soap[minorSrc]',
								 modelDesc		=	'$row_soap[modelDesc]',
								 occupDesc		=	'$row_soap[occupDesc]',
								 reference1		=	'$row_soap[reference1]',
								 reference2		=	'$row_soap[reference2]',
								 regisNm			=	'$row_soap[regisNm]',
								 regisNo				=	'$row_soap[regisNo]',
								 regisYear			=	'$row_soap[regisYear]',
								 remark				=	'$refer_no',
								 repairDesc		=	'$row_soap[repairDesc]',
								 seat					=	'$row_soap[seat]',
								 subClass			=	'$row_soap[subClass]',
								 telNo				=	'$row_soap[telNo]',
								 titleNnm			=	'$row_soap[titleNnm]',
								 tmp01				=	'$tmp01',
								 uwYear				=	'$uwYear',
								 vaddPrmm		=	'$row_soap[vaddPrmm]',
								 vbasePrmm		=	'$row_soap[vbasePrmm]',
								 vdiDiscAmt		=	'$row_soap[vdiDiscAmt]',
								 vdiscDrv			=	'$row_soap[vdiscDrv]',
								 veffectDt			=	'$row_soap[veffectDt]',
								 vexcessAmt		=	'$row_soap[vexcessAmt]',
								 vexpiryDt			=	'$row_soap[vexpiryDt]',
								 vgpDiscAmt		=	'$row_soap[vgpDiscAmt]',
								 vloadAmt			=	'$row_soap[vloadAmt]',
								 vnbDiscAmt		=	'$row_soap[vnbDiscAmt]',
								 vnetPrmm			=	'$row_soap[vnetPrmm]',
								 volClass			=	'$row_soap[volClass]',
								 voldPolicy			=	'$row_soap[voldPolicy]',
								 vpolicyNo			=	'$row_soap[vpolicyNo]',
								 vreference		=	'$row_soap[vreference]',
								 vstampDuty		=	'$row_soap[vstampDuty]',
								 vsvctaxAmt		=	'$row_soap[vsvctaxAmt]',
								 vtotalPrmm		=	'$row_soap[vtotalPrmm]',
								 VType				=	'$row_soap[VType]',
								 weight				=	'$row_soap[weight]',
								 custType			=	'$row_soap[custType]',
								 taxBrn				=	'$row_soap[taxBrn]',
								 taxBrnName		=	'$row_soap[taxBrnName]',
								 taxID				=	'$row_soap[taxID]',
								 POLICY_TRANDATE = '$date_order',	
								 POLICY_ID		= '$policy_id',	
								 STICKER_ID 		= '$sticker_id',	
								 RECEIPT_ID 		= '$receipt_id',	
								 INVOICE_ID		= '$invoice_id',	
								 vpackage_id		=	'$row_soap[vpackage_id]' ";
			mysql_query($sql_soap) or die (mysql_error());		
			//-------- / INSERT MAS POLICY ORDER ------------//
			
			//--------  SENT2GISWEB -------- //
			$sql2gis		= "select * from scil_website.mas_policy_soap where POLICY_ID='$policy_id' ";
			$query2gis	= mysql_query($sql2gis) or die (mysql_error());
			$row2gis	= mysql_fetch_array($query2gis);
			
			if(($row2gis['subClass']=="MA1") || ($row2gis['subClass']=="MA2"))  {
				try { 
					$url_gisweb = "http://61.90.141.28/GISWeb/WSGenCompulsoryPolicy/wsdl/WSGenCompulsoryPolicy.wsdl";
					$url_gisweb_test = "http://61.90.141.30/GISWeb/WSGenCompulsoryPolicy/wsdl/WSGenCompulsoryPolicy.wsdl";
								
					$sc = new SOAPClient($url_gisweb);
					
					$date_order	= 	date('Y-m-d H:i:s');
																	
					$input = new stdClass();
					$input->theInput = new stdClass();
					$input->theInput->addrAmpCd = "";
					$input->theInput->addrAmpNm = $row2gis['addrAmpNm'];;
					$input->theInput->addrBld = $row2gis['addrBld'];
					$input->theInput->addrChwCd = "";
					$input->theInput->addrChwNm = $row2gis['addrChwNm'];
					$input->theInput->addrMoo = $row2gis['addrMoo'];
					$input->theInput->addrNo = $row2gis['addrNo'];
					$input->theInput->addrPostcode = $row2gis['addrPostcode'];
					$input->theInput->addrRoad = $row2gis['addrRoad'];
					$input->theInput->addrSoi = $row2gis['addrSoi'];
					$input->theInput->addrThmCd = "";
					$input->theInput->addrThmNm = $row2gis['addrThmNm'];;
					$input->theInput->appId = $row2gis['appId'];
					$input->theInput->appType = $row2gis['appType'];
					$input->theInput->benefitNm = $row2gis['benefitNm'];
					$input->theInput->bodyDesc = $row2gis['bodyDesc']; 
					$input->theInput->cardId = $row2gis['cardId'];
					$input->theInput->carGroup = $row2gis['carGroup'];
					$input->theInput->carModify = $row2gis['carModify'];
					$input->theInput->cbarcodeRef = $row2gis['BARCODE_ID'];
					$input->theInput->cc = $row2gis['cc'];
					$input->theInput->ccovBody = $row2gis['ccovBody'];
					$input->theInput->ccovLife = $row2gis['ccovLife'];
					$input->theInput->ccovPerson = $row2gis['ccovPerson'];
					$input->theInput->ccovTimes = $row2gis['ccovTimes'];
					$input->theInput->ceffectDt = $row2gis['ceffectDt'];
					$input->theInput->cexpiryDt = $row2gis['cexpiryDt'];
					$input->theInput->chasNo = $row2gis['chasNo'];
					$input->theInput->cnetPrmm = $row2gis['cnetPrmm'];
					$input->theInput->coldPolicy = $row2gis['coldPolicy'];
					$input->theInput->colorDesc = $row2gis['colorDesc'];
					$input->theInput->comGrp = $row2gis['comGrp'];
					$input->theInput->coverCd = $row2gis['coverCd'];
					$input->theInput->cpolicyNo = $row2gis['POLICY_ID'];
					$input->theInput->creference = $row2gis['RECEIPT_ID']."/".$row2gis['INVOICE_ID'];
					$input->theInput->cstampDuty = $row2gis['cstampDuty'];
					$input->theInput->csvctaxAmt = $row2gis['csvctaxAmt'];
					$input->theInput->ctotalPrmm = $row2gis['ctotalPrmm'];
					$input->theInput->ctype = $row2gis['ctype'];
					$input->theInput->drvBirthDt1 = $row2gis['drvBirthDt1'];
					$input->theInput->drvBirthDt2 = $row2gis['drvBirthDt2'];
					$input->theInput->drvCardId1 = $row2gis['drvCardId1'];
					$input->theInput->drvCardId2 = $row2gis['drvCardId2'];
					$input->theInput->drvDriveId1 = $row2gis['drvDriveId1'];
					$input->theInput->drvDriveId2 = $row2gis['drvDriveId2'];
					$input->theInput->drvNm1 = $row2gis['drvNm1'];
					$input->theInput->drvNm2 = $row2gis['drvNm2'];
					$input->theInput->drvOccDesc1 = $row2gis['drvOccDesc1'];
					$input->theInput->drvOccDesc2 = $row2gis['drvOccDesc2'];
					$input->theInput->drvSex1 = $row2gis['drvSex1'];
					$input->theInput->drvSex2 = $row2gis['drvSex2'];
					$input->theInput->emailAddr = $row2gis['emailAddr'];
					$input->theInput->engineNo = $row2gis['engineNo'];
					$input->theInput->faxNo = $row2gis['faxNo'];
					$input->theInput->firstNm = $row2gis['firstNm'];
					
					//$input->theInput->intmCard = "ว00012/2552";
					//$input->theInput->intmId = "6400008";
					$input->theInput->intmCard = $row2gis['intmCard'];
					$input->theInput->intmId = $row2gis['intmId'];
					$input->theInput->lastNm = $row2gis['lastNm'];
					$input->theInput->mainClass = $row2gis['mainClass'];
					$input->theInput->makeDesc = $row2gis['makeDesc']; 
					$input->theInput->minorSrc = $row2gis['minorSrc'];
					$input->theInput->modelDesc = $row2gis['modelDesc'];
					$input->theInput->occupDesc = $row2gis['occupDesc'];
					$input->theInput->reference1 = $row2gis['POLICY_ID'];
					$input->theInput->reference2 = $row2gis['RECEIPT_ID'];
					$input->theInput->regisNm = $row2gis['regisNm'];
					$input->theInput->regisNo = $row2gis['regisNo'];
					$input->theInput->regisYear = $row2gis['regisYear'];
					$input->theInput->remark = $row2gis['remark'];
					$input->theInput->repairDesc = $row2gis['repairDesc'];
					$input->theInput->seat = $row2gis['seat'];
					$input->theInput->subClass = $row2gis['subClass'];
					$input->theInput->telNo = $row2gis['telNo'];
					$input->theInput->titleNnm = $row2gis['titleNnm'];
					$input->theInput->tmp01 = $row2gis['tmp01'];
					$input->theInput->uwYear = $row2gis['uwYear'];
					$input->theInput->vaddPrmm = $row2gis['vaddPrmm'];
					$input->theInput->vbasePrmm = $row2gis['vbasePrmm'];
					$input->theInput->vdiDiscAmt = $row2gis['vdiDiscAmt'];
					$input->theInput->vdiscDrv = $row2gis['vdiscDrv'];
					$input->theInput->veffectDt = $row2gis['veffectDt'];
					$input->theInput->vexcessAmt = $row2gis['vexcessAmt'];
					$input->theInput->vexpiryDt = $row2gis['vexpiryDt'];
					$input->theInput->vgpDiscAmt = $row2gis['vgpDiscAmt'];
					$input->theInput->vloadAmt = $row2gis['vloadAmt'];
					$input->theInput->vnbDiscAmt = $row2gis['vnbDiscAmt'];
					$input->theInput->vnetPrmm = $row2gis['vnetPrmm'];
					$input->theInput->volClass = $row2gis['volClass'];
					$input->theInput->voldPolicy = $row2gis['voldPolicy'];
					$input->theInput->vpolicyNo = $row2gis['vpolicyNo'];
					$input->theInput->vreference = $row2gis['vreference'];
					$input->theInput->vstampDuty = $row2gis['vstampDuty'];
					$input->theInput->vsvctaxAmt = $row2gis['vsvctaxAmt'];
					$input->theInput->vtotalPrmm = $row2gis['vtotalPrmm'];
					$input->theInput->VType = $row2gis['VType'];
					$input->theInput->weight = $row2gis['weight'];
					$input->theInput->custType = $row2gis['custType'];
					$input->theInput->taxBrn = $row2gis['taxBrn'];
					$input->theInput->taxBrnName = $row2gis['taxBrnName'];
					$input->theInput->taxID = $row2gis['taxID'];
								
					$snote = "addrAmpCd = ". $input->theInput->addrAmpCd ."\n";
					$snote .= "addrAmpNm = ". $input->theInput->addrAmpNm ."\n";
					$snote .= "addrBld = ". $input->theInput->addrBld ."\n";
					$snote .= "addrChwCd = ". $input->theInput->addrChwCd ."\n";
					$snote .= "addrChwNm = ". $input->theInput->addrChwNm ."\n";
					$snote .= "addrMoo = ". $input->theInput->addrMoo ."\n";
					$snote .= "addrNo = ". $input->theInput->addrNo ."\n";
					$snote .= "addrPostcode = ". $input->theInput->addrPostcode ."\n";
					$snote .= "addrRoad = ". $input->theInput->addrRoad ."\n";
					$snote .= "addrSoi = ". $input->theInput->addrSoi ."\n";
					$snote .= "addrThmCd = ". $input->theInput->addrThmCd ."\n";
					$snote .= "addrThmNm = ". $input->theInput->addrThmNm ."\n";
					$snote .= "appId = ". $input->theInput->appId ."\n";
					$snote .= "appType = ". $input->theInput->appType ."\n";
					$snote .= "benefitNm = ". $input->theInput->benefitNm ."\n";
					$snote .= "bodyDesc = ". $input->theInput->bodyDesc ."\n";
					$snote .= "cardId = ". $input->theInput->cardId ."\n";
					$snote .= "carGroup = ". $input->theInput->carGroup ."\n";
					$snote .= "carModify = ". $input->theInput->carModify ."\n";
					$snote .= "cbarcodeRef = ". $input->theInput->cbarcodeRef ."\n";
					$snote .= "cc = ". $input->theInput->cc ."\n";
					$snote .= "ccovBody = ". $input->theInput->ccovBody ."\n";
					$snote .= "ccovLife = ". $input->theInput->ccovLife ."\n";
					$snote .= "ccovPerson = ". $input->theInput->ccovPerson ."\n";
					$snote .= "ccovTimes = ". $input->theInput->ccovTimes ."\n";
					$snote .= "ceffectDt = ". $input->theInput->ceffectDt ."\n";
					$snote .= "cexpiryDt = ". $input->theInput->cexpiryDt ."\n";
					$snote .= "chasNo = ". $input->theInput->chasNo ."\n";
					$snote .= "cnetPrmm = ". $input->theInput->cnetPrmm ."\n";
					$snote .= "coldPolicy = ". $input->theInput->coldPolicy ."\n";
					$snote .= "colorDesc = ". $input->theInput->colorDesc ."\n";
					$snote .= "comGrp = ". $input->theInput->comGrp ."\n";
					$snote .= "coverCd = ". $input->theInput->coverCd ."\n";
					$snote .= "cpolicyNo = ". $input->theInput->cpolicyNo ."\n";
					$snote .= "creference = ". $input->theInput->creference ."\n";
					$snote .= "cstampDuty = ". $input->theInput->cstampDuty ."\n";
					$snote .= "csvctaxAmt = ". $input->theInput->csvctaxAmt ."\n";
					$snote .= "ctotalPrmm = ". $input->theInput->ctotalPrmm ."\n";
					$snote .= "ctype = ". $input->theInput->ctype ."\n";
					$snote .= "drvBirthDt1 = ". $input->theInput->drvBirthDt1 ."\n";
					$snote .= "drvBirthDt2 = ". $input->theInput->drvBirthDt2 ."\n";
					$snote .= "drvCardId1 = ". $input->theInput->drvCardId1 ."\n";
					$snote .= "drvCardId2 = ". $input->theInput->drvCardId2 ."\n";
					$snote .= "drvDriveId1 = ". $input->theInput->drvDriveId1 ."\n";
					$snote .= "drvDriveId2 = ". $input->theInput->drvDriveId2 ."\n";
					$snote .= "drvNm1 = ". $input->theInput->drvNm1 ."\n";
					$snote .= "drvNm2 = ". $input->theInput->drvNm2 ."\n";
					$snote .= "drvOccDesc1 = ". $input->theInput->drvOccDesc1 ."\n";
					$snote .= "drvOccDesc2 = ". $input->theInput->drvOccDesc2 ."\n";
					$snote .= "drvSex1 = ". $input->theInput->drvSex1 ."\n";
					$snote .= "drvSex2 = ". $input->theInput->drvSex2 ."\n";
					$snote .= "emailAddr = ". $input->theInput->emailAddr ."\n";
					$snote .= "engineNo = ". $input->theInput->engineNo ."\n";
					$snote .= "faxNo = ". $input->theInput->faxNo ."\n";
					$snote .= "firstNm = ". $input->theInput->firstNm ."\n";
					$snote .= "intmCard = ". $input->theInput->intmCard ."\n";
					$snote .= "intmId = ". $input->theInput->intmId ."\n";
					$snote .= "lastNm = ". $input->theInput->lastNm ."\n";
					$snote .= "mainClass = ". $input->theInput->mainClass ."\n";
					$snote .= "makeDesc = ". $input->theInput->makeDesc ."\n";
					$snote .= "minorSrc = ". $input->theInput->minorSrc ."\n";
					$snote .= "modelDesc = ". $input->theInput->modelDesc ."\n";
					$snote .= "occupDesc = ". $input->theInput->occupDesc ."\n";
					$snote .= "reference1 = ". $input->theInput->reference1 ."\n";
					$snote .= "reference2 = ". $input->theInput->reference2 ."\n";
					$snote .= "regisNm = ". $input->theInput->regisNm ."\n";
					$snote .= "regisNo = ". $input->theInput->regisNo ."\n";
					$snote .= "regisYear = ". $input->theInput->regisYear ."\n";
					$snote .= "remark = ". $input->theInput->remark ."\n";
					$snote .= "repairDesc = ". $input->theInput->repairDesc ."\n";
					$snote .= "seat = ". $input->theInput->seat ."\n";
					$snote .= "subClass = ". $input->theInput->subClass ."\n";
					$snote .= "telNo = ". $input->theInput->telNo ."\n";
					$snote .= "titleNnm = ". $input->theInput->titleNnm ."\n";
					$snote .= "tmp01 = ". $input->theInput->tmp01 ."\n";
					$snote .= "uwYear = ". $input->theInput->uwYear ."\n";
					$snote .= "vaddPrmm = ". $input->theInput->vaddPrmm ."\n";
					$snote .= "vbasePrmm = ". $input->theInput->vbasePrmm ."\n";
					$snote .= "vdiDiscAmt = ". $input->theInput->vdiDiscAmt ."\n";
					$snote .= "vdiscDrv = ". $input->theInput->vdiscDrv ."\n";
					$snote .= "veffectDt = ". $input->theInput->veffectDt ."\n";
					$snote .= "vexcessAmt = ". $input->theInput->vexcessAmt ."\n";
					$snote .= "vexpiryDt = ". $input->theInput->vexpiryDt ."\n";
					$snote .= "vgpDiscAmt = ". $input->theInput->vgpDiscAmt ."\n";
					$snote .= "vloadAmt = ". $input->theInput->vloadAmt ."\n";
					$snote .= "vnbDiscAmt = ". $input->theInput->vnbDiscAmt ."\n";
					$snote .= "vnetPrmm = ". $input->theInput->vnetPrmm ."\n";
					$snote .= "volClass = ". $input->theInput->volClass ."\n";
					$snote .= "voldPolicy = ". $input->theInput->voldPolicy ."\n";
					$snote .= "vpolicyNo = ". $input->theInput->vpolicyNo ."\n";
					$snote .= "vreference = ". $input->theInput->vreference ."\n";
					$snote .= "vstampDuty = ". $input->theInput->vstampDuty ."\n";
					$snote .= "vsvctaxAmt = ". $input->theInput->vsvctaxAmt ."\n";
					$snote .= "vtotalPrmm = ". $input->theInput->vtotalPrmm ."\n";
					$snote .= "VType = ". $input->theInput->VType ."\n";
					$snote .= "weight = ". $input->theInput->weight ."\n";
					$snote .= "custType = ". $input->theInput->custType ."\n";
					$snote .= "taxBrn = ". $input->theInput->taxBrn ."\n";
					$snote .= "taxBrnName = ". $input->theInput->taxBrnName ."\n";
					$snote .= "taxID = ". $input->theInput->taxID ."\n";
					
					$res = $sc->getMessage($input);
					$status = $res->getMessageReturn->status;
					$message = $res->getMessageReturn->theMessage;								
				} catch (SoapFault $e) { 
					$status = "Error";
					$message = $e->faultcode;
					$snote = $e->faultstring;
				} 

				$status = str_replace("'", "''", $status);
				$message = str_replace("'", "''", $message);
				$snote = str_replace("'", "''", $snote);
				
				//--- INSERT TXN LOG SOAP ---//
				$sql = "SELECT SENT_NO FROM scil_website.TXN_LOG_SOAP";
				$sql .= " WHERE POLICY_ID = '" . $row2gis['POLICY_ID'] ."'";
				$sql .= " ORDER BY SENT_NO DESC";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$sent_no = strval(intval($rs["SENT_NO"]) + 1);
				} else {
					$sent_no = '1';
				}
								
				$sql = "INSERT INTO scil_website.TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, AGENT_ID, LOGIN_ID, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
				$sql .= " ('".$row2gis['POLICY_ID']."', ". $sent_no .", 'POLICY', now(), '" . $_SESSION["AGENT_ID"]. "', '".$_SESSION["LOGIN_ID"]."'";
				$sql .= ", '".getenv("REMOTE_ADDR")."', '".$status."', '".$message."', '" . $snote ."')";
				mysql_unbuffered_query($sql);
								
				$subject = "WebSite Policy2GisWeb : " . $row2gis['POLICY_ID'] ." : ". $status;
						
				$to 	= "thitirat.tee@siamcityins.com, phumphat.son@siamcityins.com";
				$body = "Policy Online to GIS Web Service Log Detail<BR>";
				$body .= "------------------------------------------<BR>";
				$body .= "Policy ID : ". $row2gis['POLICY_ID'] ."<BR>";
				$body .= "Sent No. : ". $sent_no ."<BR>";
				$body .= "Sent Type : POLICY" ."<BR>";
				$body .= "Date : " . date("d/m/Y H:m") ."<BR>";
				$body .= "Agent ID : SCIL Website"."<BR>";
				$body .= "IP Address: ". getenv("REMOTE_ADDR") ."<BR>";
				$body .= "Status : ". $status ."<BR>";
				//$body .= "Message : ".iconv("utf-8", "tis-620",$message)."<BR>";
				$body .= "Message : ".$message."<BR>";
				$body .= "Note : <BR>". str_replace("\n","<BR>".$snote);
							
				SendMail("website2gis@siamcityins.com",$to, $subject, $body);
			} else {													// Voluntary
				$sql		=  "SELECT * FROM scil_website.mas_policy_soap WHERE POLICY_ID ='$policy_id' ";
				$query	= mysql_query($sql) or die (mysql_error());
				$row		= mysql_fetch_array($query);	
	
				$AddrAmpNM 	= $row['addrAmpNm'];
				$AddrBLD 			= $row['addrBld'];
				$AddrChwNM 	= $row['addrChwNm'];
				$AddrMoo 			= $row['addrMoo'];
				$AddrNo 			= $row['addrNo'];
				$AddrPostcode 	= $row['addrPostcode'];
				$AddrRoad 		= $row['addrRoad'];
				$AddrSoi 			= $row['addrSoi'];
				$AddrThmNM 	= $row['addrThmNm'];
				$AppID 				= $row['appId'];
				$AppType 			= $row['appType'];
				$BenefitNM 		= $row['benefitNm'];
				$BodyDesc 		= $row['bodyDesc'];
				$CardID 			= $row['cardId'];
				$CC 					= $row['cc'];
				$ChasNo 			= $row['chasNo'];
				$CommGRP 		= 	"Package";
				$driver1_license = $row['drvDriveId1'];
				$driver2_license = $row['drvDriveId2'];
				$driver1 			= $row['drvNm1'];
				$driver2 			= $row['drvNm2'];
				$engine_no 		= $row['engineNo'];
				$FirstNm 			= $row['firstNm'];
				$IntmCard 		= $row['intmCard'];
				$IntmID 			= $row['intmId'];
				$LastNm 			= $row['lastNm'];
				$MakeDesc 		= $row['makeDesc'];
				$MinorSRC 		= $row['minorSrc'];
				$ModelDesc 		= $row['modelDesc'];
				$RegisNm 			= $row['regisNm'];
				$RegisNo 			= $row['regisNo'];
				$RegisYear 		= $row['regisYear'];
				$Seat 				= $row['seat'];
				$Subclass 			= $row['subClass'];
				$TelNo 				= $row['telNo'];
				$TitleNm 			= $row['titleNnm'];
				$TMP01 			= $row['tmp01'];
				$UWYear 			= $row['uwYear'];
				$VExpiryDT 		= $row['vexpiryDt'];
				$VNetPrmm 		= $row['vnetPrmm'];
				$VStampDuty 	= $row['vstampDuty'];
				$VSvctaxAmt 		= $row['vsvctaxAmt'];
				$VTotalPrmm 	= $row['vtotalPrmm'];
				$Vtype 				= $row['VType'];
				$Weight 			= $row['weight'];
				$VEffectDT 		= $row['veffectDt'];
				$CustType 		= $row['custType'];
				$TaxBrn 			= $row['taxBrn'];
				$TaxBrnNm 		= $row['taxBrnName'];
				$TaxId 				= $row['taxID'];
	
				$ProdCD 				=  "GWS";
				$Status 					=  "Q";
				$VReference			=	$row['RECEIPT_ID']."/".$row['INVOICE_ID'];
				$Reference2			=	$row['RECEIPT_ID'];
				$policy_id				=	$row['POLICY_ID'];
				$invoice_id				=	$row['INVOICE_ID'];
				$receipt_id				=	$row['RECEIPT_ID'];
				$vpackage_id			=	$row['vpackage_id'];		
	
				if($Vtype == 110 && $CC > 2000){
					$pk_cc	=	" AND MOTOR_CAPACITY='>2000' ";
				}elseif($Vtype == 110 && $CC <=2000){
					$pk_cc	=	" AND MOTOR_CAPACITY='<2000' ";
				}else{
					$pk_cc	=	'';
				}
				$sql_package		=  "SELECT * FROM scil_website.mas_package_motor WHERE PACKAGE_ID='$vpackage_id' AND MOTOR_TYPE='$Vtype' $pk_cc ";
				$query_package =  mysql_query($sql_package) or die (mysql_error());
				$row_package	=  mysql_fetch_array($query_package);	
				$CoverCD 			=  $row_package['TEMP_COVER_CD'];
				$VBasePrmm 	=  $row_package['TEMP_V_BASE_PREM'];
				$VolClass 			=  $row_package['TEMP_VOL_CLASS'];
				$VaddPrmm		=  $row_package['TEMP_V_ADD_PRMM'];
				//$VDIDiscAmt 		=  $row_package['TEMP_V_DI_DISC_AMT'];
				$VNBDiscAmt 	=  $row_package['NCB'];
				$SumIns 			=  $row_package['SUM_INSURE'];
	
				$sql_group		=  "SELECT * FROM scil_website.car_model_all WHERE MODEL_NAME ='$ModelDesc' ";
				$query_group	= mysql_query($sql_group) or die (mysql_error());
				$row_group	= mysql_fetch_array($query_group);	
				$CarGroup		=	$row_group['CAR_GROUP'];
				if($CarGroup=="") {
					$CarGroup = "0";
				}
	
				if ($SumIns=="") {
					$SumIns2send	=	"0";
				}else{
					$SumIns2send	=	$SumIns;
				}
	
				// ------ SEND WEBSERVICE2GISWEB ---- //
				try { 
					$url_gisweb = "http://61.90.141.28/GISWeb/WSgenVoluntaryPolicy?wsdl";
					$url_gisweb_test = "http://61.90.141.30/GISWeb/WSgenVoluntaryPolicy?wsdl";
				
					$sc = new SOAPClient($url_gisweb);
				
					$input = new stdClass();
					$input->theInput = new stdClass();

					$input->theInput->tempAddrAmpCD= "";
					$input->theInput->tempAddrAmpNM=  $AddrAmpNM;
					$input->theInput->tempAddrBLD=  $AddrBLD;
					$input->theInput->tempAddrChwCD= "";
					$input->theInput->tempAddrChwNM= $AddrChwNM;
					$input->theInput->tempAddrMoo= $AddrMoo;
					$input->theInput->tempAddrNo=  $AddrNo;
					$input->theInput->tempAddrPostcode= $AddrPostcode;
					$input->theInput->tempAddrRoad=  $AddrRoad;
					$input->theInput->tempAddrSoi= $AddrSoi;
					$input->theInput->tempAddrThmCD= "";
					$input->theInput->tempAddrThmNM= $AddrThmNM;
					$input->theInput->tempAppID= $AppID;
					$input->theInput->tempAppType= $AppType;
					if ($benefit != "") {
						$input->theInput->tempBenefitNM= $BenefitNM;
					} else {
						$input->theInput->tempBenefitNM= $TitleNm.$FirstNm." ".$LastNm;
					}
					$input->theInput->tempBodyDesc= $BodyDesc; 
					$input->theInput->tempCardID= $CardID;
					$input->theInput->tempCarGroup= $CarGroup;
					$input->theInput->tempCarModify= "";
					$input->theInput->tempCBarcoderef= $barcode_id;
					$input->theInput->tempCC= $CC;
					$input->theInput->tempCCovBody= 0;
					$input->theInput->tempCCovLife= 0;
					$input->theInput->tempCCovPersonl= 0;
					$input->theInput->tempCCOvTimes= 0;
					$input->theInput->tempCEffectDT= "";
					$input->theInput->tempCExpiryDT= "";
					$input->theInput->tempChasNo= $ChasNo;
					$input->theInput->tempCNetPrmm= 0;
					$input->theInput->tempCOldPolicy= "";
					$input->theInput->tempColotDesc= "";
					$input->theInput->tempCommGRP= $CommGRP;
					$input->theInput->tempCoverCD= $CoverCD;
					$input->theInput->tempCPolicyNo= "";
					$input->theInput->tempCReference= "";
					$input->theInput->tempCStmapDuty= 0;
					$input->theInput->tempCSvctaxAmt= 0;
					$input->theInput->tempCTotalPrmm= 0;
					$input->theInput->tempCType= "";
					$input->theInput->tempDrvCardID1= "";
					$input->theInput->tempDrvCardID2= "";
					$input->theInput->tempDrvDriveBirthDT1= "";
					$input->theInput->tempDrvDriveBirthDT2= "";
					$input->theInput->tempDrvDriveID1= "";
					$input->theInput->tempDrvDriveID2= "";
					$input->theInput->tempDrvNM1= "";
					$input->theInput->tempDrvNM2= "";
					$input->theInput->tempDrvOccDesc1= "";
					$input->theInput->tempDrvOccDesc2= "";
					$input->theInput->tempDrvOccSex1= "";
					$input->theInput->tempDrvOccSex2= "";
					$input->theInput->tempEmailAddr= "";
					$input->theInput->tempEnginNo= "";
					$input->theInput->tempFaxNo= "";
					$input->theInput->tempFirstNm= $FirstNm;
					$input->theInput->tempIntmCard=  $IntmCard;
					$input->theInput->tempIntmID= $IntmID;
					$input->theInput->tempLastNm= $LastNm;
					$input->theInput->tempMainClass= "M";
					$input->theInput->tempMakeDesc= $MakeDesc; 
					$input->theInput->tempMinorSRC= $MinorSRC;
					$input->theInput->tempModelDesc= $ModelDesc;
					$input->theInput->tempOccupDesc= "";
					$input->theInput->tempReference1= $policy_id;
					$input->theInput->tempReference2= $Reference2;
					$input->theInput->tempRegisNm= $RegisNm;
					$input->theInput->tempRegisNo= $RegisNo;
					$input->theInput->tempRegisYear= $RegisYear;
					$input->theInput->tempRemark= "";
					$input->theInput->tempRepairDesc= "";
					$input->theInput->tempSeat= $Seat;
					$input->theInput->tempSubclass= $Subclass;
					$input->theInput->tempTelNo= $TelNo;
					if ($TitleNm == '') {
						$input->theInput->tempTitleNm = ".";
					} else {
						$input->theInput->tempTitleNm = $TitleNm;
					}
					$input->theInput->tempTMP01= $TMP01;
					$input->theInput->tempUWYear= $UWYear;
					$input->theInput->tempVaddPrmm= $VaddPrmm;
					$input->theInput->tempVBasePrmm= $VBasePrmm;
					$input->theInput->tempVDIDiscAmt= 0;
					$input->theInput->tempVDiscDrv= 0;
					$input->theInput->tempVEffectDT= $VEffectDT;
					$input->theInput->tempVExcessAmt= 0;
					$input->theInput->tempVExpiryDT= $VExpiryDT;
					$input->theInput->tempVGPDiscAmt= 0;
					$input->theInput->tempVLoadAmt= 0;
					$input->theInput->tempVNBDiscAmt= $VNBDiscAmt;
					$input->theInput->tempVNetPrmm= $VNetPrmm;
					$input->theInput->tempVolClass= $VolClass;
					$input->theInput->tempVOldPolicy= "";
					$input->theInput->tempVPolicyNo= $policy_id;
					$input->theInput->tempVReference= $VReference;
					$input->theInput->tempVStampDuty= $VStampDuty;
					$input->theInput->tempVSvctaxAmt= $VSvctaxAmt;
					$input->theInput->tempVTotalPrmm= $VTotalPrmm;
					$input->theInput->tempVType= $Vtype;
					$input->theInput->tempWeight= $Weight;
					$input->theInput->tempSumIns= $SumIns2send;
					$input->theInput->tempTaxBrn= $TaxBrn;
					$input->theInput->tempTaxBrnNm= $TaxBrnNm;
					$input->theInput->tempCustType= $CustType;
					$input->theInput->tempTaxId= $TaxId;
					$input->theInput->tempProdCD= $ProdCD;
					$input->theInput->tempStatus= $Status;
				
					$snote = "addrAmpCd = ". $input->theInput->tempAddrAmpCD."\n";
					$snote .= "addrAmpNm = ". $input->theInput->tempAddrAmpNM."\n";
					$snote .= "addrBld = ". $input->theInput->tempAddrBLD."\n";
					$snote .= "addrChwCd = ". $input->theInput->tempAddrChwCD."\n";
					$snote .= "addrChwNm = ". $input->theInput->tempAddrChwNM."\n";
					$snote .= "addrMoo = ". $input->theInput->tempAddrMoo."\n";
					$snote .= "addrNo = ". $input->theInput->tempAddrNo."\n";
					$snote .= "addrPostcode = ". $input->theInput->tempAddrPostcode."\n";
					$snote .= "addrRoad = ". $input->theInput->tempAddrRoad."\n";
					$snote .= "addrSoi = ". $input->theInput->tempAddrSoi."\n";
					$snote .= "addrThmCd = ". $input->theInput->tempAddrThmCD."\n";
					$snote .= "addrThmNm = ". $input->theInput->tempAddrThmNM."\n";
					$snote .= "appId = ". $input->theInput->tempAppID."\n";
					$snote .= "appType = ". $input->theInput->tempAppType."\n";
					$snote .= "benefitNm = ". $input->theInput->tempBenefitNM."\n";
					$snote .= "bodyDesc = ". $input->theInput->tempBodyDesc."\n";
					$snote .= "cardId = ". $input->theInput->tempCardID."\n";
					$snote .= "carGroup = ". $input->theInput->tempCarGroup."\n";
					$snote .= "carModify = ". $input->theInput->tempCarModify."\n";
					$snote .= "cbarcodeRef = ". $input->theInput->tempCBarcoderef."\n";
					$snote .= "cc = ". $input->theInput->tempCC."\n";
					$snote .= "ccovBody = ". $input->theInput->tempCCovBody."\n";
					$snote .= "ccovLife = ". $input->theInput->tempCCovLife."\n";
					$snote .= "ccovPerson = ". $input->theInput->tempCCovPersonl."\n";
					$snote .= "ccovTimes = ". $input->theInput->tempCCOvTimes."\n";
					$snote .= "ceffectDt = ". $input->theInput->tempCEffectDT."\n";
					$snote .= "cexpiryDt = ". $input->theInput->tempCExpiryDT."\n";
					$snote .= "chasNo = ". $input->theInput->tempChasNo."\n";
					$snote .= "cnetPrmm = ". $input->theInput->tempCNetPrmm."\n";
					$snote .= "coldPolicy = ". $input->theInput->tempCOldPolicy."\n";
					$snote .= "colorDesc = ". $input->theInput->tempColotDesc."\n";
					$snote .= "comGrp = ". $input->theInput->tempCommGRP."\n";
					$snote .= "coverCd = ". $input->theInput->tempCoverCD."\n";
					$snote .= "cpolicyNo = ". $input->theInput->tempCPolicyNo."\n";
					$snote .= "creference = ". $input->theInput->tempCReference."\n";
					$snote .= "cstampDuty = ". $input->theInput->tempCStmapDuty."\n";
					$snote .= "csvctaxAmt = ". $input->theInput->tempCSvctaxAmt."\n";
					$snote .= "ctotalPrmm = ". $input->theInput->tempCTotalPrmm."\n";
					$snote .= "ctype = ". $input->theInput->tempCType."\n";
					$snote .= "drvBirthDt1 = ". $input->theInput->tempDrvCardID1."\n";
					$snote .= "drvBirthDt2 = ". $input->theInput->tempDrvCardID2."\n";
					$snote .= "drvCardId1 = ". $input->theInput->tempDrvDriveBirthDT1."\n";
					$snote .= "drvCardId2 = ". $input->theInput->tempDrvDriveBirthDT2."\n";
					$snote .= "drvDriveId1 = ". $input->theInput->tempDrvDriveID1."\n";
					$snote .= "drvDriveId2 = ". $input->theInput->tempDrvDriveID2."\n";
					$snote .= "drvNm1 = ". $input->theInput->tempDrvNM1."\n";
					$snote .= "drvNm2 = ". $input->theInput->tempDrvNM2."\n";
					$snote .= "drvOccDesc1 = ". $input->theInput->tempDrvOccDesc1."\n";
					$snote .= "drvOccDesc2 = ". $input->theInput->tempDrvOccDesc2."\n";
					$snote .= "drvSex1 = ". $input->theInput->tempDrvOccSex1."\n";
					$snote .= "drvSex2 = ". $input->theInput->tempDrvOccSex2."\n";
					$snote .= "emailAddr = ". $input->theInput->tempEmailAddr."\n";
					$snote .= "engineNo = ". $input->theInput->tempEnginNo."\n";
					$snote .= "faxNo = ". $input->theInput->tempFaxNo."\n";
					$snote .= "firstNm = ". $input->theInput->tempFirstNm."\n";
					$snote .= "intmCard = ". $input->theInput->tempIntmCard."\n";
					$snote .= "intmId = ". $input->theInput->tempIntmID."\n";
					$snote .= "lastNm = ". $input->theInput->tempLastNm."\n";
					$snote .= "mainClass = ". $input->theInput->tempMainClass."\n";
					$snote .= "makeDesc = ". $input->theInput->tempMakeDesc."\n";
					$snote .= "minorSrc = ". $input->theInput->tempMinorSRC."\n";
					$snote .= "modelDesc = ". $input->theInput->tempModelDesc."\n";
					$snote .= "occupDesc = ". $input->theInput->tempOccupDesc."\n";
					$snote .= "reference1 = ". $input->theInput->tempReference1."\n";
					$snote .= "reference2 = ". $input->theInput->tempReference2."\n";
					$snote .= "regisNm = ". $input->theInput->tempRegisNm."\n";
					$snote .= "regisNo = ". $input->theInput->tempRegisNo."\n";
					$snote .= "regisYear = ". $input->theInput->tempRegisYear."\n";
					$snote .= "remark = ". $input->theInput->tempRemark."\n";
					$snote .= "repairDesc = ". $input->theInput->tempRepairDesc."\n";
					$snote .= "seat = ". $input->theInput->tempSeat."\n";
					$snote .= "subClass = ". $input->theInput->tempSubclass."\n";
					$snote .= "telNo = ". $input->theInput->tempTelNo."\n";
					$snote .= "titleNnm = ". $input->theInput->tempTitleNm."\n";
					$snote .= "tmp01 = ". $input->theInput->tempTMP01."\n";
					$snote .= "uwYear = ". $input->theInput->tempUWYear."\n";
					$snote .= "vaddPrmm = ". $input->theInput->tempVaddPrmm."\n";
					$snote .= "vbasePrmm = ". $input->theInput->tempVBasePrmm."\n";
					$snote .= "vdiDiscAmt = ". $input->theInput->tempVDIDiscAmt."\n";
					$snote .= "vdiscDrv = ". $input->theInput->tempVDiscDrv."\n";
					$snote .= "veffectDt = ". $input->theInput->tempVEffectDT."\n";
					$snote .= "vexcessAmt = ". $input->theInput->tempVExcessAmt."\n";
					$snote .= "vexpiryDt = ". $input->theInput->tempVExpiryDT."\n";
					$snote .= "vgpDiscAmt = ". $input->theInput->tempVGPDiscAmt."\n";
					$snote .= "vloadAmt = ". $input->theInput->tempVLoadAmt."\n";
					$snote .= "vnbDiscAmt = ". $input->theInput->tempVNBDiscAmt."\n";
					$snote .= "vnetPrmm = ". $input->theInput->tempVNetPrmm."\n";
					$snote .= "volClass = ". $input->theInput->tempVolClass."\n";
					$snote .= "voldPolicy = ". $input->theInput->tempVOldPolicy."\n";
					$snote .= "vpolicyNo = ". $input->theInput->tempVPolicyNo."\n";
					$snote .= "vreference = ". $input->theInput->tempVReference."\n";
					$snote .= "vstampDuty = ". $input->theInput->tempVStampDuty."\n";
					$snote .= "vsvctaxAmt = ". $input->theInput->tempVSvctaxAmt."\n";
					$snote .= "vtotalPrmm = ". $input->theInput->tempVTotalPrmm."\n";
					$snote .= "VType = ". $input->theInput->tempVType."\n";
					$snote .= "weight = ". $input->theInput->tempWeight."\n";
					$snote .= "SumIns = ". $input->theInput->tempSumIns."\n";
					$snote .= "taxBrn = ". $input->theInput->tempTaxBrn."\n";
					$snote .= "taxBrnName = ". $input->theInput->tempTaxBrnNm."\n";
					$snote .= "custType = ". $input->theInput->tempCustType."\n";
					$snote .= "taxID = ". $input->theInput->tempTaxId."\n";
					$snote .= "ProdCD = ". $input->theInput->tempProdCD."\n";
					$snote .= "Status = ". $input->theInput->tempStatus."\n";
				
					$res = $sc->getMessage($input);
					$status = $res->getMessageReturn->status;
					$message = $res->getMessageReturn->message;			
				} catch (SoapFault $e) { 
					$status = "Error";
					$message = $e->faultcode;
						$snote = $e->faultstring;
				} 
				$status = str_replace("'", "''", $status);
				$message = str_replace("'", "''", $message);
					$snote = str_replace("'", "''",     $snote);
				$sql = "SELECT SENT_NO FROM scil_website.TXN_LOG_SOAP";
				$sql .= " WHERE POLICY_ID = '" . $policy_id ."'";
				$sql .= " ORDER BY SENT_NO DESC";
				$result = mysql_query($sql);
				if ($rs = mysql_fetch_array($result)) {
					$sent_no = strval(intval($rs["SENT_NO"]) + 1);
				} else {
					$sent_no = '1';
				}
			
				if ($status=="SUCCESS") {
					$status2db = "1";
					$message = "Success:บันทึกข้อมูลเรียบร้อย";
				} else {
					$status2db = "0";
				}
			
				$sql = "INSERT INTO scil_website.TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, AGENT_ID, LOGIN_ID, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
				$sql .= " ('".$policy_id."', ". $sent_no .", 'GISWeb_VOL', now(), '" . $_SESSION["AGENT_ID"]. "', '".$_SESSION["LOGIN_ID"]."'";
				$sql .= ", '".getenv("REMOTE_ADDR")."', '".$status2db."', '".$message."', '" .     $snote ."')";
				mysql_unbuffered_query($sql);
		
				$subject = "WebSite Policy2GisWeb : " . $policy_id ." : ". $status;
			
				$to 	= "thitirat.tee@siamcityins.com, phumphat.son@siamcityins.com";
				$body = "Policy Online to GIS Web Service Log Detail<BR>";
				$body .= "------------------------------------------<BR>";
				$body .= "Policy ID : ". $policy_id ."<BR>";
				$body .= "Sent No. : ". $sent_no ."<BR>";
				$body .= "Sent Type : POLICY" ."<BR>";
				$body .= "Date : " . date("d/m/Y H:m") ."<BR>";
				$body .= "IP Address: ". getenv("REMOTE_ADDR") ."<BR>";
				$body .= "Status : ". $status ."<BR>";
				$body .= "Message : ". $message ."<BR>";
				$body .= "Note : <BR>". str_replace("\n","<BR>",     $snote);
		
				SendMail("website2gis@siamcityins.com",$to, $subject, $body);
			}

			// Sent E-Policy
			$url = 'https://www.siamcityinsurance.com/ws_new/download_policy_motor/E-Policy.php'; // กำหนด URl ของ API
				
			$user		= 'SCI_Website';
			$password	= 'Si@m2o20';
			$pol			= array("policy_id"=>$policy_id);
			$request	= json_encode($pol);
		  
			$ch = curl_init(); // เริ่มต้นใช้งาน cURL
			  
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($ch, CURLOPT_USERPWD, $user.":".$password);
			  
			$response = curl_exec($ch); 
			curl_close($ch); 
		}
	}
}
echo $data_json;
?>